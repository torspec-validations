namespace :tt do
  desc "Generate Ruby parsers from Treetop grammars."
  task :gen do
    task_path = File.expand_path('../../torspec/**/*.treetop', __FILE__)
    Dir.glob(task_path) do |filename|
      `bundle exec tt #{filename}`
    end
  end
end
