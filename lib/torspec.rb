module Torspec
  grammars = %w[ primitives ]
  grammars.each do |g|
    begin
      # If there is a pre-compiled grammar, load it.
      require 'treetop/runtime'
      require "torspec/#{g}"
    rescue LoadError
      # Otherwise, compile the grammar with Treetop."
      require 'treetop/runtime'
      require 'treetop/compiler'
      Treetop.load(File.join(File.dirname(__FILE__) + "/torspec/#{g}"))
    end
  end
end
