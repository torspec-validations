Gem::Specification.new do |s|
  s.name        = "torspec"
  s.version     = "0.1.0"
  s.author      = "Poet (Tim Sally)"
  s.email       = "poet@stack.io"
  s.homepage    = "http://www.atomicpeace.com"
  s.description = "Grammars matching the Tor Spec."
  s.add_dependency('treetop', '>= 1.4.8')
  s.files = %w(README.txt LICENSE.txt Rakefile) + Dir.glob("lib/**/*")
end
