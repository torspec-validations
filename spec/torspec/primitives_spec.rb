require 'spec_helper'

require 'treetop/compiler'
Treetop.load(File.join(TORSPEC_ROOT, 'lib/torspec/primitives'))

describe "PrimitivesGrammar" do
  it "should parse a valid DateTime value" do
    text   = "2011-04-01 22:10:01"
    parser = Torspec::PrimitivesParser.new
    nodes  = parser.parse(text, :root => :DateTime)
    nodes.should_not be_nil
  end

  it "should not parse a invalid DateTime value" do
    text   = "2011-04-01 25r:10:01"
    parser = Torspec::PrimitivesParser.new
    nodes  = parser.parse(text, :root => :DateTime)
    nodes.should be_nil
  end

end
