require 'spec_helper'

require 'treetop/compiler'
Treetop.load(File.join(TORSPEC_ROOT, 'lib/torspec/dir_spec_v2/network_status'))

describe "NetworkStatusGrammar" do
  before(:each) do
    @parser = Torspec::DirspecV2::NetworkStatusParser.new
  end

  describe "Preamble" do
    it "should parse a valid network status version" do
      text  = "network-status-version 2"
      nodes = @parser.parse(text, :root => :NetworkStatusVersion)
      nodes.should_not be_nil    

    end

    it "should parse a valid directory source" do
      text  = "dir-source tor.dizum.com 194.109.206.212 80"
      nodes = @parser.parse(text, :root => :DirSourceLine)
      nodes.should_not be_nil    
    end

    it "should parse a valid fingerprint line" do
      text  = "fingerprint 7EA6EAD6FD83083C538F44038BBFA077587DD755"
      nodes = @parser.parse(text, :root => :FingerprintLine)
      nodes.should_not be_nil          
    end

    it "should parse a valid published line" do
      text = "published 2007-08-01 00:38:25"
      nodes = @parser.parse(text, :root => :PublishedLine)
      nodes.should_not be_nil          
    end

  end
end
