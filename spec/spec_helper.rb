require File.expand_path('../environment', __FILE__)

unless defined?(TORSPEC_ROOT)
  TORSPEC_ROOT = File.join(File.dirname(__FILE__), '../')
end

unless defined?(SPEC_ROOT)
  SPEC_ROOT = File.join(File.dirname(__FILE__))
end

require 'torspec'
