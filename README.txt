A validation suite for Tor's protocols. Grammars for each protocol written from
the specifications. Ruby parsers are generated from these grammars and unit
tested to give some confidence in the correctness of the validation suite.

Please contact the author at <poet@stack.io> with comments and suggestions.

